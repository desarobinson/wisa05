<?php

namespace Modules\Item\Models;

use App\Models\Tenant\Item;
use App\Models\Tenant\ModelTenant;

class Nombreagente extends ModelTenant
{

    protected $fillable = [ 
        'nombre',

    ];
 
    public function items()
    {
        return $this->hasMany(Item::class);
    }
    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }
 
   

}