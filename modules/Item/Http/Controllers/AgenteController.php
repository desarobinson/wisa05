<?php

namespace Modules\Item\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Item\Models\Agente;
use Modules\Item\Models\Cashagente;
use Modules\Item\Models\Nombreagente;
use Modules\Item\Http\Resources\AgenteCollection;
use Modules\Item\Http\Resources\AgenteResource;
use Modules\Item\Http\Requests\AgenteRequest;
use App\Models\Tenant\User;
class AgenteController extends Controller
{

    public function index()
    {
        return view('item::agentes.index');
    }


    public function columns()
    {
        return [
            'operacion' => 'Operacion',
            'banks' => 'Banco/Caja',
        ];
    }

    public function records(Request $request)
    {
        $records = Agente::where($request->column, 'like', "%{$request->value}%")
                            ->latest();

        return new AgenteCollection($records->paginate(config('tenant.items_per_page')));
    }
    public function tables() {  

        $idagente = Cashagente::where('user_id', '=','1')->get();
        $agentett = Agente::query()->orderBy('operacion')->get()
                                ->transform(function($row) {
                                    return [
                                        'id' => $row->id,
                                        'nombre' => $row->operacion,
                                    ];
                                });

        return compact('idagente','agentett');
    }

    public function record($id)
    {
        $record = Agente::findOrFail($id);

        return $record;
    }

    public function store(AgenteRequest $request)
    {
        
        $first_user = User::select('id')->first();
        $r = Cashagente::select('id')->where('estado', 1)->where('user_id', $first_user['id'])->get();
        $id = $request->input('id');
        $agente = Agente::firstOrNew(['id' => $id]);
        $agente->fill($request->all());
        $agente->id_cash = $r[0]['id'];
        $agente->user_id = $first_user['id'];
        Log::info($first_user['id']);
        $agente->save();


        return [
            'success' => true,
            'message' => ($id)?'Agente editada con éxito':'Agente registrada con éxito',
            'data' => $agente

        ];

    }

    public function destroy($id)
    {
        try {

            $agente = Agente::findOrFail($id);
            $agente->delete();

            return [
                'success' => true,
                'message' => 'Agente eliminada con éxito'
            ];

        } catch (Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => "La Agente esta siendo usada por otros registros, no puede eliminar"] : ['success' => false,'message' => "Error inesperado, no se pudo eliminar la categoría"];

        }

    }




}
