<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Item\Models\Agente;
use Modules\Item\Models\Cashagente;
use Modules\Item\Models\Nombreagente;
use Modules\Item\Http\Resources\NombreagenteCollection;
use Modules\Item\Http\Resources\NombreagenteResource;
use Modules\Item\Http\Requests\NombreagenteRequest;
use App\Models\Tenant\User;
class NombreagenteController extends Controller
{

    public function index()
    {
        return view('item::nombreagentes.index');
    }


    public function columns()
    {
        return [
            'nombre' => 'Nombre',
          
        ];
    }

    public function records(Request $request)
    {
        $records = Nombreagente::where($request->column, 'like', "%{$request->value}%")
                            ->latest();

        return new NombreagenteCollection($records->paginate(config('tenant.items_per_page')));
    }
    public function tables() {  

        $idagente = Cashagente::where('user_id', '=','1')->get();
        $agentett = Nombreagente::query()->orderBy('nombre')->get()
                                ->transform(function($row) {
                                    return [
                                        'id' => $row->id,
                                        'nombre' => $row->nombre,
                                    ];
                                });

        return compact('idagente','agentett');
    }

    public function record($id)
    {
        $record = Nombreagente::findOrFail($id);

        return $record;
    }

    public function store(NombreagenteRequest $request)
    {
        $id = $request->input('id');
        $agente = Nombreagente::firstOrNew(['id' => $id]);
        $agente->fill($request->all());
        $agente->save();


        return [
            'success' => true,
            'message' => ($id)?'Categoría editada con éxito':'Categoría registrada con éxito',
            'data' => $agente

        ];

    }

    public function destroy($id)
    {
        try {

            $agente = Nombreagente::findOrFail($id);
            $agente->delete();

            return [
                'success' => true,
                'message' => 'Categoría eliminada con éxito'
            ];

        } catch (Exception $e) {

            return ($e->getCode() == '23000') ? ['success' => false,'message' => "La categoría esta siendo usada por otros registros, no puede eliminar"] : ['success' => false,'message' => "Error inesperado, no se pudo eliminar la categoría"];

        }

    }




}
